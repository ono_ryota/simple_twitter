package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet("/deleteMessage")
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//getParameterの値はStringになるのでintに変換
		Integer deleteMessageId = Integer.parseInt(request.getParameter("deleteMessageId"));
		Message message = new Message();
		message.setId(deleteMessageId);

		new MessageService().delete(message);
		response.sendRedirect("./");
	}
}
