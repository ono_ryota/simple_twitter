package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet("/edit")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		User loginUser = (User)session.getAttribute("loginUser");
		request.setAttribute("loginUser", loginUser);

		String StringEditMessageId = request.getParameter("editMessageId");
		Message editMessage = null;
		if(!StringUtils.isEmpty(StringEditMessageId) && StringEditMessageId.matches("^[0-9]*$")) {
			Integer editMessageId = Integer.parseInt(StringEditMessageId);
			editMessage = new MessageService().select(editMessageId);
		}

		if(editMessage == null) {
			errorMessages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("editMessage", editMessage);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();

		Message editMessage = getMessage(request);
		if(isValid(editMessage, errorMessages)) {
			try {
				new MessageService().update(editMessage);
			} catch (NoRowsUpdatedRuntimeException e) {
	    		 errorMessages.add("更新されています。最新のデータを表示しました。データを確認してください。");
	    	}
		}

		if(errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("editMessage", editMessage);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
	    	return;
		}
		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {
		Message message = new Message();

		message.setId(Integer.parseInt(request.getParameter("editMessageId")));
		message.setText(request.getParameter("text"));
		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {
		String text = message.getText();
		if(StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		} else if(140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
